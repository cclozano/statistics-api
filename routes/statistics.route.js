const express = require('express');
const {
  getStatistics,
  createStatistic
} = require('../controllers/statistics.controller.js');

const router = express.Router({ mergeParams: true });

router
  .route('/')
  .get(getStatistics)
  .post(createStatistic);

module.exports = router;
