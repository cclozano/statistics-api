const mongoose = require('mongoose');
var Schema = mongoose.Schema;

const StatisticSchema = new Schema({
    name: {
        type: String,
        required: [true, 'Please add a name']
    },
    missed: {
        type: String,
        required: [true, 'Please add a number']
    },
    met: {
        type: String,
        required: [true, 'Please add a number']
    }
});

module.exports = mongoose.model('Statistic', StatisticSchema);
