# statistics-api

API para integrar las estadisticas en el portal de MOMENTUM

1. Install all dependencies: ``` npm install``` 
2. Create environment variables file:  ``` touch config/config.env``` 
3. Paste content on config.env
4. Populate database: ``` npm run seed ```
5. Start development server: ``` npm run dev```
