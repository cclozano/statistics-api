const ErrorResponse = require('../utils/errorResponse');
const asyncHandler = require('../middleware/async');
const Statistic = require('../models/statistic.model');


exports.getStatistics = asyncHandler(async (req, res, next) => {
  const statistics = await Statistic.find({});
  res.status(200).json({
    success: true,
    data: statistics
  });
});


exports.createStatistic = asyncHandler(async (req, res, next) => {
  const statistic = await Statistic.create(req.body);

  res.status(201).json({
    success: true,
    data: statistic
  });
});
